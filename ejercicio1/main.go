package main

import (
	"fmt"
	"sort"
	"strings"
)

func main() {

	// text string definition
	names := "Luis,Camilo,Andres,Laura,Ernesto,Andrea"

	// slip to separate the text string
	name := strings.Split(names, ",")

	// sort alphabetically
	sort.Sort(sort.StringSlice(name))

	// len to count the total data
	count := len(name)

	// responses
	fmt.Println("names: ", name)
	fmt.Println("The number is: ", count)
}
