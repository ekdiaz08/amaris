package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
)

type Pokemon struct {
	FormName     string        `json:"form_name"`
	FormNames    []interface{} `json:"form_names"`
	FormOrder    int           `json:"form_order"`
	ID           int           `json:"id"`
	IsBattleOnly bool          `json:"is_battle_only"`
	IsDefault    bool          `json:"is_default"`
	IsMega       bool          `json:"is_mega"`
	Name         string        `json:"name"`
	Names        []interface{} `json:"names"`
	Order        int           `json:"order"`
	Pokemon      struct {
		Name string `json:"name"`
		URL  string `json:"url"`
	} `json:"pokemon"`
	Sprites struct {
		BackDefault      string      `json:"back_default"`
		BackFemale       interface{} `json:"back_female"`
		BackShiny        string      `json:"back_shiny"`
		BackShinyFemale  interface{} `json:"back_shiny_female"`
		FrontDefault     string      `json:"front_default"`
		FrontFemale      interface{} `json:"front_female"`
		FrontShiny       string      `json:"front_shiny"`
		FrontShinyFemale interface{} `json:"front_shiny_female"`
	} `json:"sprites"`
	Types []struct {
		Slot int `json:"slot"`
		Type struct {
			Name string `json:"name"`
			URL  string `json:"url"`
		} `json:"type"`
	} `json:"types"`
	VersionGroup struct {
		Name string `json:"name"`
		URL  string `json:"url"`
	} `json:"version_group"`
}

func main() {

	// Insert new id
	id := 10

	// query http service
	url := fmt.Sprintf("https://pokeapi.co/api/v2/pokemon-form/%d", id)
	resp, err := http.Get(url)

	if err != nil {
		log.Printf("Request Failed: %s", err)
		return
	}

	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Printf("Reading body failed: %s", err)
		return
	}

	// read body and put each field in its place
	posts := Pokemon{}
	err = json.Unmarshal(body, &posts)
	if err != nil {
		log.Printf("Reading body failed: %s", err)
		return
	}

	log.Printf("the name of the pokemon is:  %s", posts.Name)
}
