package main

import (
	"fmt"
	"strings"
)

func main() {
	x := "TOKYO"
	y := "KYOTO"

	if Friends(x, y) {
		fmt.Printf("Las cadenas '%s' y '%sy' son amigas", x, y)
	} else {
		fmt.Printf("Las cadenas '%s' y '%s' NO son amigas", x, y)
	}
}

func Friends(x, y string) bool {
	if len(x) != len(y) {
		return false
	}
	for i := 0; i < len(x); i++ {
		v := string(x[1])
		u := string(x[1])
		if strings.EqualFold(u, v) {
			return true
		}
	}
	return false
}
